// console.log("hello")


/**
 * Instructions that can be provided to the students for reference:
Activity:
6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7. Create another condition that if the current value is divisible by 5, print the number.
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.
13. Create a git repository named S20.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.
 */

let num = Number(prompt("Give me a number: "));
console.log("The number provide is: " + num);
for(num;num>=0;num--){
    
   if(num <= 50){
     console.log("The current value is at 50. Terminating the loop.");
     break;
    }

   if(num % 10 === 0){
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
    }else if(num % 5 === 0){
        console.log(num)
    } 
}

let varString = "supercalifragilisticexpialidocious";
 
 let resultString = "";
 console.log(varString);

   for(let i = 0; i < varString.length; i++){
    if(varString[i] === "a" || varString[i] === "e" || varString[i] === "i" || varString[i] === "o" || varString[i] === "u"){
    
     continue;
   }else{
    resultString +=varString[i]
   }
    
   }
  console.log(resultString);
   